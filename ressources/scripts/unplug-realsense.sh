#!/bin/bash

port="2-4" # as shown by lsusb -t: {bus}-{port}(.{subport})

bind_usb() {
  echo "$1" >/sys/bus/usb/drivers/usb/bind
}

unbind_usb() {
  echo "$1" >/sys/bus/usb/drivers/usb/unbind
}

if test `id -u` -ne 0
then
	echo "Need sudo permission, run sudo $0 $*"
	exit 1
fi


unplug=1
if test $# -eq 1 && test "$1" == "plug"
then
	unplug=0
fi

case $unplug in
	0) bind_usb $port;;
	1) unbind_usb $port;;
esac
