#!/bin/bash

if test $# -ne 1 || test "$1" == "-h" || test "$1" == "--help"
then
	echo "usage : $0 kernel-version : Uninstall the specified kernel if installed"
	echo "        $0 --list         : List the installed kernel versions"
	echo "        $0 -h, --help     : Display this help and exit"
	exit 1
fi

current=$(uname -r)

if test "$1" == "--list"
then
	for kernel in $(ls /boot/vmlinuz-*|cut -d '-' -f 2-)
	do
		if test "$kernel" == "$current"
		then
			str="(current, do not remove !)"
		fi
		echo $kernel $str
	done
	exit 0
fi

directories="/boot/vmlinuz-
/boot/initrd.img-
/boot/System-map-
/boot/config-
/lib/modules/
/var/lib/initramfs-tools/
/boot/abi-
/boot/retpoline-"

version=$1

if test -e "/boot/vmlinuz-$version"
then
	if test "$version" == "$current"
	then
		echo "Error : $version is the current kernel, refusing to uninstall it !"
		exit 1
	fi
	echo "Are you sure to completly remove kernel version $version ? [y/N]"
	read confirm
	if test "$confirm" != "y"
	then
		echo Aborted
		exit 0
	fi
	for d in $directories
	do
		path=$d$version
		if test -e $path
		then
			sudo rm -rf $path
		fi
	done;
	sudo update-grub2
else
	echo "Version $version does not seem to exists"
fi

