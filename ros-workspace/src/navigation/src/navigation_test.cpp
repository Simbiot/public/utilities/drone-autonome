#include <ros/ros.h>
#include <navigation/commande_drone.hpp>

int main(int argc, char** argv)
{
  ros::init(argc, argv, "navigation_test", ros::init_options::NoSigintHandler);
  ros::NodeHandle nh;

  CommandeDrone commande(nh);
  commande.enableDegugLog();

  int estimatorType = (int) commande.getFCUParam(PX4_PARAM_ID_ESTIMATOR_TYPE);
  int estimatorMode = (int) commande.getFCUParam(PX4_PARAM_ID_EKF2_BITMASK);
  int estimatorHeightMode = (int) commande.getFCUParam(PX4_PARAM_ID_EKF2_HEIGHT_MODE);
  int estimatorDelay = (int) commande.getFCUParam(PX4_PARAM_ID_EKF2_DELAY);

  std::string estimatorModeStr =  std::string(estimatorMode & PX4_PARAM_EKF2_BM_GPS ? "GPS (pos), " : "") +
                                  std::string(estimatorMode & PX4_PARAM_EKF2_BM_GPS_HEADING ? "GPS (heading), " : "") +
                                  std::string(estimatorMode & PX4_PARAM_EKF2_BM_OPTIFLOW ? "OptiFlow, " : "") +
                                  std::string(estimatorMode & PX4_PARAM_EKF2_BM_VISION_POS ? "vision (pos), " : "") +
                                  std::string(estimatorMode & PX4_PARAM_EKF2_BM_VISION_HEADING ? "vision (heading), " : "") +
                                  std::string(estimatorMode & PX4_PARAM_EKF2_BM_MULTIROTOR_DRAG ? "multirotor drag, " : "") +
                                  std::string(estimatorMode & PX4_PARAM_EKF2_BM_ROTATE_COORDS ? "rotated coordinate system" : "");

  std::string estimatorHeightModeStr = estimatorHeightMode == PX4_PARAM_EKF2_HEIGHT_GPS ? "GPS" :
                                       estimatorHeightMode == PX4_PARAM_EKF2_HEIGHT_BARO ? "Barometer" :
                                       estimatorHeightMode == PX4_PARAM_EKF2_HEIGHT_RANGE ? "Range sensor" :
                                       estimatorHeightMode == PX4_PARAM_EKF2_HEIGHT_VISION ? "Vision" : "Unknown";


  ROS_INFO(" ---------- FCU Parameters ---------- ");
  ROS_INFO("%-25s : %s", "Estimator type", estimatorType == PX4_PARAM_ESTIMATOR_LPE ? "LPE" : "EKF2");
  ROS_INFO("%-25s : %s", "Estimator mode", estimatorModeStr.c_str());
  ROS_INFO("%-25s : %s", "Estimator height mode", estimatorHeightModeStr.c_str());
  ROS_INFO("%-25s : %d ms", "Estimator delay", estimatorDelay);
  ROS_INFO("%-25s : %d standard deviations", "Gate size", (int) commande.getFCUParam(PX4_PARAM_ID_EKF2_VISION_GATE));
  ROS_INFO("%-25s : %s", "Sideslip mes. if no GPS", commande.getFCUParam(PX4_PARAM_ID_EFK2_FUSE_BETA) ? "true" : "false");
  ROS_INFO("%-25s : %.3f", "Vision angle noise", (float) commande.getFCUParam(PX4_PARAM_ID_EKF2_VISION_HEADING_NOISE));
  ROS_INFO("%-25s : %.3f", "Vision position noise", (float) commande.getFCUParam(PX4_PARAM_ID_EKF2_VISION_POS_NOISE));
  ROS_INFO(" ---------- -------------- ---------- ");

  std::string estimatorTopic = nh.resolveName("estimated_pose");

  commande.setEstimationPreferences(false, false);
  commande.registerEstimatorTopic(estimatorTopic, true);

  ros::Duration(4).sleep();

  if (!commande.init())
  {
    ROS_ERROR("Impossible d'initialiser le guidage du drone !");
    return 1;
  }

  float min = 1, max = 2.3;
  char buffer[2];

  ROS_INFO("Taking off ...");
  commande.takeoff(2);
  commande.setYaw(0, 0);

 ROS_INFO("Takeoff completed, going to pos (0,0,2) ...");
  commande.goToPosition((Coords){0, 0, 2});
  ROS_INFO("Pos (0,0,2) reached, landing ...");
  commande.land();
  ROS_INFO("Landed !");

 commande.takeoff(min);

Coords points[] = {
	(Coords){-0.5, 1.4, min},
        (Coords){1, 1.4, min},
        (Coords){1, -2.2, min},
        (Coords){-0.5, -2.2, min},
        (Coords){-0.5, 1.4, max},
        (Coords){1, 1.4, max},
        (Coords){1, -2.2, max},
        (Coords){-0.5, -2.2, max}
};

  for (Coords point : points)
{
	commande.setPosition(point);
	ros::Duration(3).sleep();
	if (!commande.ok())
		break;
}

  commande.setPosition((Coords){0, 0, (min + max) / 2});

  if (commande.ok())
   std::cin.getline(buffer, 1);

  commande.land();

  ROS_INFO("Fin du programme");
  return 0;
}
