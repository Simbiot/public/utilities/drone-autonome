/**
* This file is part of DSO.
*
* Copyright 2016 Technical University of Munich and Intel.
* Developed by Jakob Engel <engelj at in dot tum dot de>,
* for more information see <http://vision.in.tum.de/dso>.
* If you use this code, please cite the respective publications as
* listed on the above website.
*
* DSO is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DSO is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DSO. If not, see <http://www.gnu.org/licenses/>.
*/


#include <locale.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "util/settings.h"
#include "FullSystem/FullSystem.h"
#include "util/Undistort.h"

#ifndef IGNORE_PANGOLIN
	#include "IOWrapper/Pangolin/PangolinDSOViewer.h"
#endif
#include "IOWrapper/OutputWrapper/SampleOutputWrapper.h"


#include <ros/ros.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/Int8.h>
#include <sensor_msgs/CameraInfo.h>
#include <geometry_msgs/PoseStamped.h>
#include "cv_bridge/cv_bridge.h"
#include "OutputWrapper.hpp"


std::string calib = "";
std::string vignetteFile = "";
std::string gammaFile = "";
std::string saveFile = "";
std::string calibDir="src/dso_ros/calib";
bool useSampleOutput=false;
ros::Publisher pubState;

using namespace dso;

void parseArgument(char* arg)
{
	int option;
	char buf[1000];
	useSampleOutput = true;
	if(1==sscanf(arg,"sampleoutput=%d",&option))
	{
		if(option==0)
		{
			useSampleOutput = false;
			printf("NOT USING SAMPLE OUTPUT WRAPPER!\n");
		}
		return;
	}

	if(1==sscanf(arg,"quiet=%d",&option))
	{
		if(option==1)
		{
			setting_debugout_runquiet = true;
			setting_logStuff = false;
			printf("QUIET MODE, I'll shut up!\n");
		}
		return;
	}

	if(1==sscanf(arg,"nogui=%d",&option))
	{
		if(option==1)
		{
			disableAllDisplay = true;
			printf("NO GUI!\n");
		}
		return;
	}
	if(1==sscanf(arg,"nomt=%d",&option))
	{
		if(option==1)
		{
			multiThreading = false;
			printf("NO MultiThreading!\n");
		}
		return;
	}
	if(1==sscanf(arg,"calib=%s",buf))
	{
		calib = buf;
		printf("loading calibration from %s!\n", calib.c_str());
		return;
	}
	if(1==sscanf(arg,"vignette=%s",buf))
	{
		vignetteFile = buf;
		printf("loading vignette from %s!\n", vignetteFile.c_str());
		return;
	}

	if(1==sscanf(arg,"gamma=%s",buf))
	{
		gammaFile = buf;
		printf("loading gammaCalib from %s!\n", gammaFile.c_str());
		return;
	}

	if(1==sscanf(arg,"savefile=%s",buf))
	{
		saveFile = buf;
		printf("saving to %s on finish!\n", saveFile.c_str());
		return;
	}
	if (1 == sscanf(arg, "calibDir=%s", buf))
	{
		calibDir = buf;
		printf("loading calibration files from %s\n", calibDir.c_str());
	}
	printf("could not parse argument \"%s\"!!\n", arg);
}




FullSystem* fullSystem = 0;
Undistort* undistorter = 0;
int frameID = 0;

/**
* Fonction pour remettre à 0 l'odometry
*/
void reset(){
	std::vector<IOWrap::Output3DWrapper*> wraps = fullSystem->outputWrapper;
	delete fullSystem;
	for(IOWrap::Output3DWrapper* ow : wraps) ow->reset();
	fullSystem = new FullSystem();
	fullSystem->linearizeOperation=false;
	fullSystem->outputWrapper = wraps;
		if(undistorter->photometricUndist != 0)
			fullSystem->setGammaFunction(undistorter->photometricUndist->getG());
	setting_fullResetRequested=false;
}

/**
* Call back des images prises
*/
void vidCb(const sensor_msgs::ImageConstPtr img)
{
	cv_bridge::CvImageConstPtr cv_ptr = cv_bridge::toCvShare(img, sensor_msgs::image_encodings::MONO8);
	assert(cv_ptr->image.type() == CV_8U);
	assert(cv_ptr->image.channels() == 1);
	//si user a demandé à reset (gui)
	if(setting_fullResetRequested)
	{
		reset();
		frameID=0;
	}
	//traitement de la nouvelle image
	MinimalImageB minImg((int)cv_ptr->image.cols, (int)cv_ptr->image.rows,(unsigned char*)cv_ptr->image.data);
	ImageAndExposure* undistImg = undistorter->undistort<unsigned char>(&minImg, 1,0, 1.0f);
	undistImg->timestamp=img->header.stamp.toSec(); // relay the timestamp to dso
	fullSystem->addActiveFrame(undistImg, frameID);
	frameID++;
	std_msgs::Int8 msg;
	//Si le signal a été lost
	if(fullSystem->isLost){
		ROS_INFO("Perte du signal, reset auto");
		msg.data=2;
		frameID=0;
	}
	//En cours dinit
	else if(!fullSystem->initialized){
		msg.data=0;
	}
	else{
		msg.data=1;
	}
	pubState.publish(msg);
	delete undistImg;

}

/**
* Chargement des différents fichiers de calibration
*/
void loadCalibrationFiles()
{
	if (calib.empty())
		calib = calibDir + "/camera.txt";
	if (gammaFile.empty())
		gammaFile = calibDir + "/pcalib.txt";
	/*if (vignetteFile.empty())
		vignetteFile = calibDir + "/vignette.png";
		*/
}

void callbackState(const std_msgs::Int8& state){
	if(state.data == 1){
		//On ne fait rien
	}
	else if(state.data == 0){
		ROS_INFO("Initialisation en cours");
	}
	else{
		reset();
	}
}

/**
* Main principal de l'application
*/
int main( int argc, char** argv )
{
	ros::init(argc, argv, "dso_live");

	for(int i=1; i<argc;i++) parseArgument(argv[i]);

	loadCalibrationFiles();

	ROS_INFO("début");
	setting_desiredImmatureDensity = 1000;
	setting_desiredPointDensity = 1200;
	setting_minFrames = 5;
	setting_maxFrames = 7;
	setting_maxOptIterations=4;
	setting_minOptIterations=1;
	setting_logStuff = false;
	setting_kfGlobalWeight = 1.3;


	ROS_INFO("MODE WITH CALIBRATION, but without exposure times!\n");
	setting_photometricCalibration = 2;
	setting_affineOptModeA = 0;
	setting_affineOptModeB = 0;

  ros::NodeHandle nh;

	if (calib.empty())
	{
		ROS_ERROR("No calibration file supplied !");
		return 1;
	}
	if (gammaFile.empty())
	{
		ROS_ERROR("No gamma file supplied !");
		return 1;
	}

  undistorter = Undistort::getUndistorterForFile(calib, gammaFile, vignetteFile);

	if (!undistorter)
	{
		ROS_ERROR("Unable to create undistorter from calib (%s), gamma (%s), and vignette (%s, optional)", calib.c_str(), gammaFile.c_str(), vignetteFile.c_str());
		return 1;
	}

  setGlobalCalib(
          (int)undistorter->getSize()[0],
          (int)undistorter->getSize()[1],
          undistorter->getK().cast<float>());


  fullSystem = new FullSystem();
  fullSystem->linearizeOperation=false;

	#ifndef IGNORE_PANGOLIN
  if(!disableAllDisplay)
    fullSystem->outputWrapper.push_back(new IOWrap::PangolinDSOViewer(
    		 (int)undistorter->getSize()[0],
    		 (int)undistorter->getSize()[1]));
		#endif


  if(useSampleOutput)
      fullSystem->outputWrapper.push_back(new OutputWrapper(nh));


  if(undistorter->photometricUndist != 0)
  	fullSystem->setGammaFunction(undistorter->photometricUndist->getG());

	ros::NodeHandle n;
	//Publishing frequency is equal to camera topic frequency (with RealSense rgb default values, it is 30 Hz)
  ros::Subscriber imgSub = nh.subscribe("image", 1, &vidCb);
	//Subscriber sur le reset
	ros::Subscriber stateSub = nh.subscribe("dso/state", 1, &callbackState);
	pubState = n.advertise<std_msgs::Int8>("dso/state",10);

  ros::spin();
  fullSystem->printResult(saveFile);
  for(IOWrap::Output3DWrapper* ow : fullSystem->outputWrapper)
  {
      ow->join();
      delete ow;
  }

  delete undistorter;
  delete fullSystem;

	return 0;
}
