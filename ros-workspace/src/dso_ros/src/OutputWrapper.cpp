/**
* Wrapper 3d dso ros perso
*/

#include "boost/thread.hpp"
#include "util/MinimalImage.h"
#include "IOWrapper/Output3DWrapper.h"

#include "FullSystem/HessianBlocks.h"
#include "util/FrameShell.h"
#include "OutputWrapper.hpp"

using namespace dso;

OutputWrapper::OutputWrapper(ros::NodeHandle& n)
{
    pubPose = n.advertise<geometry_msgs::Pose>("dso/pose", 10);
    pubState = n.advertise<std_msgs::Int8>("dso/state",10);
    ROS_INFO("OUT: Created SampleOutputWrapper\n");
}

OutputWrapper::~OutputWrapper()
{
    ROS_INFO("OUT: Destroyed SampleOutputWrapper\n");
}

void OutputWrapper::publishGraph(const std::map<uint64_t, Eigen::Vector2i, std::less<uint64_t>, Eigen::aligned_allocator<std::pair<const uint64_t, Eigen::Vector2i>>> &connectivity)
{

}

void OutputWrapper::publishKeyframes( std::vector<FrameHessian*> &frames, bool final, CalibHessian* HCalib)
{

}

void OutputWrapper::publishCamPose(FrameShell* frame, CalibHessian* HCalib)
{
 geometry_msgs::Pose pose;
 auto matrix = frame->camToWorld.matrix3x4();
 //Generation du Point
 geometry_msgs::Point point;
 point.x=matrix.col(3)(0);
 point.y=matrix.col(3)(2);
 point.z=-matrix.col(3)(1);
 //Generation du quaternion eigen
 Eigen::Matrix3f m;
 for(int i = 0; i<3;i++){
   for(int j = 0;j<3;j++){
     m(i,j) = matrix.col(j)(i);
   }
 }
 Eigen::Quaternionf q(m);
 //adaptation en un quaternion message
 geometry_msgs::Quaternion quaternion;
 quaternion.x = q.x();
 quaternion.y = q.y();
 quaternion.z = q.z();
 quaternion.w = q.w();
 //on renseigne les deux informations sur la pose
 pose.position = point;
 pose.orientation = quaternion;
 //Vérification que les points ne soient pas trop éloignées (> 0.4 de distance)
 if(dernierPts.size() > 1){
   float mX = 0;
   float mY = 0;
   float mZ = 0;
   for(int i = 1 ; i<dernierPts.size();i++){
     mX+= fabs(fabs(dernierPts.at(i-1).x)- fabs(dernierPts.at(i).x));
     mY+= fabs(fabs(dernierPts[i-1].y) - fabs(dernierPts[i].y));
     mZ+= fabs(fabs(dernierPts[i-1].z) - fabs(dernierPts[i].z));
   }
   mX = mX/(dernierPts.size()-1);
   mY = mY/(dernierPts.size()-1);
   mZ = mZ/(dernierPts.size()-1);
   geometry_msgs::Point lastPts = dernierPts.front();
   float lastEcartX = fabs(fabs(point.x)-fabs(lastPts.x));
   float lastEcartY = fabs(fabs(point.y)-fabs(lastPts.y));
   float lastEcartZ = fabs(fabs(point.z)-fabs(lastPts.z));
   if(lastEcartX > mX*30){
     ROS_INFO("Déplacement trop important sur X, recalibration!");
   }
   else if(lastEcartY > mY*30){
     ROS_INFO("Déplacement trop important sur Y, recalibration!");
   }
   else if(lastEcartZ > mZ*30){
     ROS_INFO("Déplacement trop important sur Z, recalibration!");
   }
   if(lastEcartX > mX*30 ||lastEcartY > mY*30 ||lastEcartZ > mZ*30){
     std_msgs::Int8 msg;
     msg.data=4;
     pubState.publish(msg);
   }
   if(dernierPts.size() == 10 ){
     dernierPts.pop_back();
   }
 }
 dernierPts.push_front(point);
 pubPose.publish(pose);
}

void OutputWrapper::pushLiveFrame(FrameHessian* image)
{
    // can be used to get the raw image / intensity pyramid.
}

void OutputWrapper::pushDepthImage(MinimalImageB3* image)
{
    // can be used to get the raw image with depth overlay.
}

bool OutputWrapper::needPushDepthImage()
{
    return false;
}

void OutputWrapper::pushDepthImageFloat(MinimalImageF* image, FrameHessian* KF )
{

}
