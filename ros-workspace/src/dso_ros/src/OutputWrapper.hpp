#ifndef OUTPUT_WRAPPER_INCLUDED
#define OUTPUT_WRAPPER_INCLUDED
/**
* Wrapper 3d dso ros perso
*/


#include <ros/ros.h>
#include <cmath>
#include <string>
#include <cmath>
#include <deque>
#include "boost/thread.hpp"
#include "util/MinimalImage.h"
#include "IOWrapper/Output3DWrapper.h"
#include <std_msgs/Int8.h>
#include "FullSystem/HessianBlocks.h"
#include "util/FrameShell.h"
#include "sophus/sophus.hpp"
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Quaternion.h>

using namespace dso;

class OutputWrapper : public IOWrap::Output3DWrapper
{
public:
        OutputWrapper(ros::NodeHandle& n);

        virtual ~OutputWrapper();

        virtual void publishGraph(const std::map<uint64_t, Eigen::Vector2i, std::less<uint64_t>, Eigen::aligned_allocator<std::pair<const uint64_t, Eigen::Vector2i>>> &connectivity) override;

        virtual void publishKeyframes( std::vector<FrameHessian*> &frames, bool final, CalibHessian* HCalib) override;

        virtual void publishCamPose(FrameShell* frame, CalibHessian* HCalib) override;

        virtual void pushLiveFrame(FrameHessian* image) override;

        virtual void pushDepthImage(MinimalImageB3* image) override;

        virtual bool needPushDepthImage() override;

        virtual void pushDepthImageFloat(MinimalImageF* image, FrameHessian* KF ) override;

private:
  ros::Publisher pubPose;
  ros::Publisher pubState;
  std::deque<geometry_msgs::Point> dernierPts;
};

#endif
