#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseWithCovariance.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <std_srvs/Empty.h>
#include <mutex>

std::string frame;
std::mutex lock;
bool reset = false;

void poseStampCallback(const geometry_msgs::PoseStampedConstPtr& poseStamped, std::vector<geometry_msgs::PoseStamped>& poses)
{
  lock.lock();
  poses.push_back(*poseStamped);
  lock.unlock();
}

void poseCallback(const geometry_msgs::PoseConstPtr& pose, std::vector<geometry_msgs::PoseStamped>& poses)
{
  geometry_msgs::PoseStampedPtr poseStamped = boost::make_shared<geometry_msgs::PoseStamped>();
  poseStamped->header.stamp = ros::Time::now();
  poseStamped->header.frame_id = frame;
  poseStamped->pose = *pose;
  poseStampCallback(poseStamped, poses);
}

void poseCovStampCallback(const geometry_msgs::PoseWithCovarianceStampedConstPtr& poseCovStamped, std::vector<geometry_msgs::PoseStamped>& poses)
{
  geometry_msgs::PoseStampedPtr poseStamped = boost::make_shared<geometry_msgs::PoseStamped>();
  poseStamped->header = poseCovStamped->header;
  poseStamped->pose = poseCovStamped->pose.pose;
  poseStampCallback(poseStamped, poses);
}

void poseCovCallback(const geometry_msgs::PoseWithCovarianceConstPtr& poseCov, std::vector<geometry_msgs::PoseStamped>& poses)
{
  poseCallback(boost::make_shared<geometry_msgs::Pose>(poseCov->pose), poses);
}

bool resetCB(std_srvs::Empty::Request& req, std_srvs::Empty::Response& rep) {
  reset = true;
  return true;
}



int main(int argc, char** argv)
{
  ros::init(argc, argv, "visualisation_mouvement");
  ros::NodeHandle nh;
  ros::NodeHandle relativeNode("~");

  bool stamped = false;
  bool covariance = false;
  int publishRate = 20;

  relativeNode.getParam("isStamped", stamped);
  relativeNode.getParam("hasCovariance", covariance);
  relativeNode.getParam("publishRate", publishRate);
  relativeNode.getParam("frame_id", frame);


  std::string poseTopic = nh.resolveName("pose");
  if (poseTopic == "/pose")
    ROS_WARN_STREAM("Pose topic has not been remapped, subscribing to topic " + poseTopic);

  std::vector<geometry_msgs::PoseStamped> poses;

  ros::ServiceServer service = relativeNode.advertiseService("reset", resetCB);

  ros::Subscriber poseSub;

  if (stamped && covariance)
    poseSub = nh.subscribe<geometry_msgs::PoseWithCovarianceStamped>(poseTopic, 5, boost::bind(poseCovStampCallback, _1, boost::ref(poses)));
  else if (!stamped && covariance)
    poseSub = nh.subscribe<geometry_msgs::PoseWithCovariance>(poseTopic, 5, boost::bind(poseCovCallback, _1, boost::ref(poses)));
  else if (stamped && !covariance)
    poseSub = nh.subscribe<geometry_msgs::PoseStamped>(poseTopic, 5, boost::bind(poseStampCallback, _1, boost::ref(poses)));
  else
    poseSub = nh.subscribe<geometry_msgs::Pose>(poseTopic, 5, boost::bind(poseCallback, _1, boost::ref(poses)));

  ros::Publisher pub = nh.advertise<nav_msgs::Path>("chemin", 5);

  ros::Rate rateLimiter(publishRate);
  while(ros::ok())
  {
    if (reset) {
      reset = false;
      poses.clear();
    }

    lock.lock();
    nav_msgs::Path chemin;
    chemin.header.stamp = ros::Time::now();
    chemin.header.frame_id = frame;
    chemin.poses = poses;
    pub.publish(chemin);
    lock.unlock();

    ros::spinOnce();
    rateLimiter.sleep();
  }

  return 0;
}
