#include <cmath>

#include <ros/ros.h>
#include <mavros_msgs/RCIn.h>


void afficherEtat(const mavros_msgs::RCInConstPtr& manetteData)
{
  const int MIN_VALUE = 1000, MAX_VALUE = 2000;
  const int CHANNEL_THROTTLE = 2, CHANNEL_YAW = 3, CHANNEL_ROLL = 0, CHANNEL_PITCH = 1;

  char buffer[11];

  int percentage = 100 * manetteData->rssi / 255;
  int nb = percentage / 10;

  int n;
  for (n = 0; n < nb; n++) buffer[n] = '*';
  buffer[n] = 0;

  ROS_INFO("Force du signal : [%-10s] %d %%", buffer, percentage);


  char manette[5][21] = {
    "   ---        ---   ",
    " --   --    --   -- ",
    "-       -  -       -",
    " --   --    --   -- ",
    "   ---        ---   ",
  };
  int xMax = 3, yMax = 2, xMin = -3, yMin = -2, milieuXGauche = 4, milieuXDroite = 15, milieuY = 2;

  float vals[4];
  for (int i = 0; i < 4; i++) {
    vals[i] = 1 - ((float) manetteData->channels[i] - MIN_VALUE) / (MAX_VALUE - MIN_VALUE);
  }

  int xGauche = milieuXGauche + (int) std::round(vals[CHANNEL_YAW] * (xMax - xMin)) + xMin, yGauche = milieuY + (int) std::round(vals[CHANNEL_THROTTLE] * (yMax - yMin)) + yMin;
  int xDroite = milieuXDroite + (int) std::round(vals[CHANNEL_ROLL] * (xMax - xMin)) + xMin, yDroite = milieuY + (int) std::round(vals[CHANNEL_PITCH] * (yMax - yMin)) + yMin;


  manette[yGauche][xGauche] = 'O';
  manette[yDroite][xDroite] = 'O';

  for (int i = 0; i < 5; i++)
    ROS_INFO("%s", manette[i]);
  ROS_INFO(" ");

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "visualisation_manette");
  ros::NodeHandle nh;

  ros::Subscriber motorsSub = nh.subscribe<mavros_msgs::RCIn>("/mavros/rc/in", 10, afficherEtat);

  ros::spin();

  return 0;
}
