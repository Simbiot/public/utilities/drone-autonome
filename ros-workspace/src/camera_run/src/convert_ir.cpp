#include "ros/ros.h"
#include "cv_bridge/cv_bridge.h"
#include "sensor_msgs/Image.h"

using namespace std;

void cvtCallback(const sensor_msgs::ImageConstPtr& msg, const ros::Publisher& pub)
{
  cv_bridge::CvImagePtr image = cv_bridge::toCvCopy(msg, "");
  image->encoding = "mono8";

  sensor_msgs::ImagePtr cvtMsg = image->toImageMsg();
  cvtMsg->header = msg->header;

  pub.publish(cvtMsg);
}

int main(int argc, char **argv)
{
  //initialize node
  ros::init(argc, argv, "analyse_image_exemple");

  // node handler
  ros::NodeHandle n;

  // subsribe topic
  ros::Publisher pub1 = n.advertise<sensor_msgs::Image>("/camera/ir/converted_image_raw", 5);
  ros::Publisher pub2 = n.advertise<sensor_msgs::Image>("/camera/ir2/converted_image_raw", 5);

  ros::Subscriber sub = n.subscribe<sensor_msgs::Image>("/camera/ir/image_raw", 5, boost::bind(&cvtCallback, _1, pub1));
  ros::Subscriber sub2 = n.subscribe<sensor_msgs::Image>("/camera/ir2/image_raw", 5, boost::bind(&cvtCallback, _1, pub2));

  ros::spin();

  return 0;
}
