#include "ros/ros.h"
#include <string>
#include <linux/videodev2.h>
#include "v4l2_obj.h"
#include <opencv2/opencv.hpp>
#include <image_transport/image_transport.h>
#include "cv_bridge/cv_bridge.h"

/**
* Main du noeud de la camera bottom
*/

ros::Publisher pub;

/**
*Callback pour les photos
*/
int callBackPrisePhoto(const void * buffer, int size){
  //on fait une copy du buffer pour l'utiliser avec opencv
  void * copyBuffer = malloc(size);
  memcpy(copyBuffer, buffer, size);
  //On convertit le buffer avec un format d'image YUV en un format RGB opencv
  cv::Mat picYV12 = cv::Mat(1080 * 3/2, 1920, CV_8UC1, copyBuffer);
  cv::Mat image;
  cv::cvtColor(picYV12, image, CV_YUV2BGR_YV12);
  //On crée le message image à partir de l'image opencv
  sensor_msgs::Image img_pub;
  std_msgs::Header header;
  header.stamp = ros::Time::now();
  cv_bridge::CvImage img_bridge = cv_bridge::CvImage(header,sensor_msgs::image_encodings::RGB8,image);
  img_bridge.toImageMsg(img_pub);
  //On publie
  pub.publish(img_pub);
  //On libere l'espace mémoire
  free(copyBuffer);
  return 0;
}

int main(int argc, char **argv)
{
  //Init ros + publisher
  ros::init(argc,argv,"camera_front");
  ros::NodeHandle n, n_private("~");
  pub = n.advertise<sensor_msgs::Image> ("camera/front/image_raw", 1);
  //fréquence en sec (par défault = 0.1)
  float frequence = 10;
  if (!n_private.getParam("frequence", frequence))
    ROS_INFO_STREAM("Pas de paramètre de frequence d'images, par default " << frequence << " par seconde => Modifiez le parametre ros " << ros::this_node::getNamespace() << "/" << ros::this_node::getName() << "/frequence si vous voulez changer cette frequence");
  ROS_INFO("Frequence choisie : %f",frequence);
  ros::Rate rate(frequence);
  //Init cam
  CV4l2Obj camera(CAMERA_OV8858, callBackPrisePhoto);

  camera.flip(FLIP_VERTICAL | FLIP_HORIZONTAL);
  camera.set_exposure(4500, 16, 1024);
  //camera.set_fps(30);
  camera.start_capturing();
  //Boucle
  while(ros::ok()){
    //on passe par le callback 1 fois
    camera.mainloop(1);
    ros::spinOnce();
    rate.sleep();
  }
}
