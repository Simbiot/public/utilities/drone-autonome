#include "ros/ros.h"
#include <iostream>
#include "cv_bridge/cv_bridge.h"
#include "sensor_msgs/Image.h"
#include <opencv2/opencv.hpp>
#include <image_transport/image_transport.h>

using namespace std;

int sizeErode,sizeDilate,nbOrdres;

void traitementOrdre(std::vector<cv::Point> milieu,int larg,int hauteur)
{
  nbOrdres++;
  string ordre = "";
  cv::Point milieuFenetre = cv::Point(larg/2,hauteur/2);
  for(int i = 0 ; i<milieu.size();i++){
    int x = milieu[i].x;
    int y = milieu[i].y;
    if( (x < milieuFenetre.x+(0.2*larg) && x > milieuFenetre.x-(0.2 * larg)) && (y < milieuFenetre.y+(0.2*larg) && y > milieuFenetre.y-(0.2 * larg))){
      ordre+="recule!!!!";
    }
  }
  if(ordre==""){
    ROS_INFO("Pas d'ordres");
  }
  else{
    ROS_INFO("%s", ordre.c_str());
  }
}

void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
  //Image 1 => GaussianBlur
  cv::Mat image = cv_bridge::toCvCopy(msg, "")->image;
  cv::Mat image2, imagergb(image.rows, image.cols, CV_8UC3, cv::Scalar(0, 0, 0));

  image.convertTo(imagergb, CV_8UC1);
  cv::cvtColor(imagergb, imagergb, CV_GRAY2RGB);
  image.copyTo(image2);
  //On augment les valeurs
  image*=255;
  //On inverse les blancs/noirs
  cv::bitwise_not(image, image);
  //On lisse les valeurs
  cv::GaussianBlur(image, image, (cv::Size){5, 5}, 0);

  //Element nécessaire à erode et dilate
  cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(sizeErode, sizeErode));
  cv::Mat element2 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(sizeDilate, sizeDilate));

  //Image 2 => erode / dilate

  image2*=255;
  cv::bitwise_not(image2, image2);
  cv::erode(image2, image2, element);
  cv::dilate(image2, image2, element2);
    image2/=255;

  cv::Mat imgContour;

  image2.convertTo(imgContour, CV_8UC1);
  std::vector<std::vector<cv::Point> > contours;
  std::vector<cv::Vec4i> hierarchy;
  cv::findContours(imgContour, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE);
  std::vector<cv::Point> ptsMilieu;
  for(int i = 0; i < contours.size(); i++) {
    cv::drawContours(imagergb,contours,i,(cv::Scalar){255, 0, 255}, cv::FILLED);
    cv::Moments mom = moments(contours[i]);
    int x = mom.m10/mom.m00;
    int y = mom.m01/mom.m00;
    cv::Point pt = cv::Point(x,y);
    ptsMilieu.push_back(pt);
    cv::putText(imagergb,"milieu",cv::Point(x-20,y-20),cv::FONT_HERSHEY_SIMPLEX,0.5,(cv::Scalar){255, 255, 255},2);
    cv::circle( imagergb, pt, 6, (cv::Scalar){0, 255, 255}, cv::FILLED);
  }
  traitementOrdre(ptsMilieu,imagergb.cols,imagergb.rows);
  /// Show in a window

  //on affiche les trois images
  cv::imshow( "image", image );
  cv::imshow( "image2", imagergb );
  //cv::imshow( "image3", image3 );
  //on wait
  cv::waitKey(1);
}

int main(int argc, char **argv)
{
  //initialize node
  ros::init(argc, argv, "analyse_image_distance");
  ros::NodeHandle n;

  // subsribe topic
  ros::Subscriber sub = n.subscribe("/camera/depth/image_raw", 1, imageCallback);
  //ros::Subscriber sub = n.subscribe("/camera/depth/image_raw", 1, imageCallback);

  cv::namedWindow("image2");

  //Init default window
  //Parametres de base de gestion
  sizeErode = 36;
  sizeDilate = 46;

  //Create trackbars for parameter
  cv::createTrackbar("Taille erode", "image2", &sizeErode, 70);
  cv::createTrackbar("Taille dilate", "image2", &sizeDilate, 70);
  // node handler

  ros::spin();
  nbOrdres=0;

  return 0;
}
