#include "ros/ros.h"
#include <iostream>
#include "cv_bridge/cv_bridge.h"
#include "sensor_msgs/Image.h"
#include <opencv2/opencv.hpp>
#include <image_transport/image_transport.h>

using namespace std;

int saturationMin, saturationMax, valueMin, valueMax;

void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
  cv::Mat image = cv_bridge::toCvShare(msg, "bgr8")->image;
  cv::Mat led_blanche;
  std::vector<std::vector<cv::Point> > contours;
  std::vector<cv::Vec4i> hierarchy;

  cv::cvtColor(image, led_blanche, cv::COLOR_RGB2HSV);

  //mat, min_hsv, max_hsv, dst
  cv::inRange(led_blanche, (cv::Scalar){0, saturationMin, valueMin}, (cv::Scalar){255, saturationMax, valueMax}, led_blanche);

  cv::findContours(led_blanche, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE);

  if (contours.size())
  {
    int minRadius = 3, maxRadius = 12;
    for(int idx = 0 ; idx >= 0; idx = hierarchy[idx][0] )
    {
        int minX = -1, minY = -1, maxX = -1, maxY = -1;

        for(int p = 0; p < contours[idx].size(); p++)
        {
          int x = contours[idx][p].x, y = contours[idx][p].y;
          if (minX == -1 || minX > x) minX = x;
          if (minY == -1 || minY > y) minY = y;
          if (maxX == -1 || maxX < x) maxX = x;
          if (maxY == -1 || maxY < y) maxY = y;
        }

        float centerX = (minX + maxX) / 2, centerY = (minY + maxY) / 2;
        int w = maxX - minX, h = maxY - minY;
        float avgRadius = (w + h) / 4;

        if (avgRadius < minRadius || avgRadius > maxRadius)
          continue;

        cv::rectangle(image, cv::Point(minX, minY), cv::Point(maxX, maxY), (cv::Scalar){0, 255, 0});
        cv::circle(image, cv::Point(centerX, centerY), avgRadius, (cv::Scalar){0, 0, 255}, 2);
    }
  }

  cv::imshow( "image", image );
  cv::imshow( "led", led_blanche );
  cv::waitKey(1);
}

int main(int argc, char **argv)
{
  //initialize node
  ros::init(argc, argv, "analyse_image_exemple");

  //Init default window
  cv::namedWindow("image");

  //Set default value to parameters
  saturationMin = 0 ;
  saturationMax = 10 ;
  valueMin = 240;
  valueMax = 255;

  //Create trackbars for parameters
  cv::createTrackbar("Saturation low threshold", "image", &saturationMin, 255);
  cv::createTrackbar("Saturation high thresold", "image", &saturationMax, 255);
  cv::createTrackbar("Value low threshold     ", "image", &valueMin, 255);
  cv::createTrackbar("Value high thresold     ", "image", &valueMax, 255);
  // node handler
  ros::NodeHandle n;

  // subsribe topic
  ros::Subscriber sub = n.subscribe("/camera/rgb/image_raw", 1, imageCallback);

  ros::spin();

  return 0;
}
