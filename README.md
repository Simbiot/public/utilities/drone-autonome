Drone autonome
==============

#### Programmation de drones pour la navigation autonome dans des mines abandonnées et la récupération de données grâce à un réseau de capteurs

### Se réferer au [wiki](https://gitlab.inria.fr/Simbiot/common/team-only/users/interns/duval-moreliere-dut-2019/drone-autonome/wikis/home) pour plus d'informations

Description
----

 Ce projet vise à développer un système de navigation autonome pour des drones de surveillance dans des mines abandonnées. Ces drones ont pour mission de collecter les données d'un réseau de capteur en évitant les obstacles et se repérant dans la mine.


- Drone utilisé :          Intel Aero RTF

- Système d’exploitation : Ubuntu 16.04 LTS

- Contrôleur de vol :      PX4 1.9.0

- Version de ROS :         Kinetic


Build
----

Après avoir cloné le projet, se rendre dans le dossier *ros-worskpace* : `cd ros-workspace`

Lancer l'outil de build catkin : `catkin_make` ou `catkin build`

Les packages que nous avons développé sont les packages `analyse_image`, `calibration`, `camera_run`, `dso_ros` (en partie), `navigation` et `visualisation`.
Les autres packages sont des outils que nous avons testé ou utilisé.
